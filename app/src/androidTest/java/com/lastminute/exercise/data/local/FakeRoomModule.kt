package com.lastminute.exercise.hotels.data.local

import android.content.Context
import androidx.room.Room
import com.lastminute.exercise.hotels.data.local.database.HotelsDatabase
import com.lastminute.exercise.hotels.data.local.di.RoomModule
import dagger.Module
import dagger.Provides
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import dagger.hilt.testing.TestInstallIn

@Module
@TestInstallIn(
    components = [SingletonComponent::class],
    replaces = [RoomModule::class]
)
class FakeRoomModule {

    @Provides
    fun provideHotelsDatabase(@ApplicationContext context: Context): HotelsDatabase {
        return Room.databaseBuilder(context, HotelsDatabase::class.java, DATABASE_NAME)
            .build()
    }

    companion object {
        private const val DATABASE_NAME = "hotels-test"
    }
}