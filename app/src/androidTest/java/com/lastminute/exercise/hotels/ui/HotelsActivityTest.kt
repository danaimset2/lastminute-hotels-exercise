package com.lastminute.exercise.hotels.ui

import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.lastminute.exercise.HotelsActivity
import com.lastminute.exercise.R
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@HiltAndroidTest
@RunWith(AndroidJUnit4::class)
class HotelsActivityTest {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Test
    fun test_navigation_start_destination_is_hotelsFragment() {
        val navController = TestNavHostController(ApplicationProvider.getApplicationContext())
        val hotelsScenario = ActivityScenario.launch(HotelsActivity::class.java)
        hotelsScenario.onActivity {
            navController.setGraph(R.navigation.hotels_graph)
        }

        Assert.assertEquals(navController.currentDestination?.id, R.id.hotelsFragment)
    }
}