package com.lastminute.exercise.hotels.ui.details

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.lastminute.exercise.R
import com.lastminute.exercise.TestUtils
import com.lastminute.exercise.hotels.data.local.HotelsLocalDataSource
import com.lastminute.exercise.launchFragmentInHiltContainer
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import javax.inject.Inject

@HiltAndroidTest
@RunWith(AndroidJUnit4::class)
class HotelDetailsFragmentTest {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Inject
    lateinit var hotelsLocalDataSource: HotelsLocalDataSource

    @Before
    fun setUp() = runBlocking {
        hiltRule.inject()

        with(hotelsLocalDataSource) {
            clearAll()
            saveHotels(listOf(TestUtils.generateHotel(1)))
        }
    }

    @Test
    fun test_hotel_details_display_all_data() {
        val arguments = HotelDetailsFragmentArgs(1).toBundle()
        launchFragmentInHiltContainer<HotelDetailsFragment>(arguments)

        // TODO: Provide custom view matcher for collapsing toolbar(title)
        //onView(with("Cortez")).check(matches(isDisplayed()))
        onView(withText("317 South Spring Street"))
            .check(matches(isDisplayed()))
        onView(withText("€ 100")).check(matches(isDisplayed()))
        onView(withText("(5.0)")).check(matches(isDisplayed()))
        onView(withText("Los Angeles")).check(matches(isDisplayed()))
        onView(ViewMatchers.withId(R.id.ratingBar)).check(matches(isDisplayed()))
        onView(withText("00:00")).check(matches(isDisplayed()))
        onView(withText("12:00")).check(matches(isDisplayed()))
        onView(withText("14:00")).check(matches(isDisplayed()))
        onView(withText("20:00")).check(matches(isDisplayed()))
        onView(withText("danaimset@gmail.com")).check(matches(isDisplayed()))
        onView(withText("+14154006845")).check(matches(isDisplayed()))
    }
}