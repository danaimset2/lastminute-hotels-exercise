package com.lastminute.exercise.hotels.ui.list

import androidx.navigation.Navigation
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.lastminute.exercise.R
import com.lastminute.exercise.TestUtils
import com.lastminute.exercise.hotels.data.local.HotelsLocalDataSource
import com.lastminute.exercise.launchFragmentInHiltContainer
import com.lastminute.exercise.utils.EspressoIdlingResource
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import javax.inject.Inject

@HiltAndroidTest
@RunWith(AndroidJUnit4::class)
class HotelsFragmentTest {

    @Inject
    lateinit var hotelsLocalDataSource: HotelsLocalDataSource

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Before
    fun registerIdlingResource() {
        IdlingRegistry.getInstance().register(EspressoIdlingResource.countingIdlingResource)
    }

    @After
    fun unregisterIdlingResource() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.countingIdlingResource)
    }

    @Before
    fun setUp() = runBlocking {
        hiltRule.inject()
        with(hotelsLocalDataSource) {
            clearAll()
            saveHotels(listOf(TestUtils.generateHotel()))
        }
    }

    @Test
    fun test_hotel_item_display_all_data() {
        launchFragmentInHiltContainer<HotelsFragment>()
        onView(withText("Cortez")).check(matches(isDisplayed()))
        onView(withText("317 South Spring Street")).check(matches(isDisplayed()))
        onView(withText("€ 100")).check(matches(isDisplayed()))
        onView(withText("5.0")).check(matches(isDisplayed()))
        onView(withText("Los Angeles")).check(matches(isDisplayed()))
        onView(withId(R.id.ratingBar)).check(matches(isDisplayed()))
    }

    @Test
    fun test_navigation_to_hotel_details() {
        val navController = TestNavHostController(
            ApplicationProvider.getApplicationContext()
        )

        launchFragmentInHiltContainer<HotelsFragment> {
            navController.setGraph(R.navigation.hotels_graph)
            Navigation.setViewNavController(requireView(), navController)
        }
        onView(withId(R.id.hotelsRecycler))
            .perform(actionOnItemAtPosition<HotelItemViewHolder>(0, click()))
        assertEquals(navController.currentDestination?.id, R.id.hotelDetailsFragment)
    }
}