package com.lastminute.exercise.data.models

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class HotelsWithHolidayPackages(
    val hotels: List<Hotel>,
    val holidayPackages: List<HolidayPackage>
)