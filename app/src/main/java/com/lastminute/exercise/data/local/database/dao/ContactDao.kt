package com.lastminute.exercise.hotels.data.local.database.dao

import androidx.room.Dao
import androidx.room.Insert
import com.lastminute.exercise.hotels.data.local.database.entities.ContactEntity

@Dao
interface ContactDao {

    @Insert
    suspend fun insertAll(contacts: List<ContactEntity>)
}