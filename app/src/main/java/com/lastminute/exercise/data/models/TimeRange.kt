package com.lastminute.exercise.data.models

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TimeRange(
    val from: String,
    val to: String
)
