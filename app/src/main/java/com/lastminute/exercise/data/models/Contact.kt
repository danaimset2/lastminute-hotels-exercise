package com.lastminute.exercise.data.models

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Contact(
    val phoneNumber: String,
    val email: String
)
