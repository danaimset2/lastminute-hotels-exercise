package com.lastminute.exercise.data.models

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Location(
    val address: String,
    val city: String,
    val latitude: Double,
    val longitude: Double
)
