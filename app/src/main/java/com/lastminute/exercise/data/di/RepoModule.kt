package com.lastminute.exercise.hotels.data.di

import com.lastminute.exercise.data.HotelsRepository
import com.lastminute.exercise.data.HotelsRepositoryImpl
import com.lastminute.exercise.hotels.data.local.HotelsLocalDataSource
import com.lastminute.exercise.hotels.data.local.HotelsLocalDataSourceImpl
import com.lastminute.exercise.data.remote.HotelsRemoteDataSource
import com.lastminute.exercise.data.remote.HotelsRemoteDataSourceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface RepoModule {

    @Binds
    fun bindLocalDataSource(dataSource: HotelsLocalDataSourceImpl): HotelsLocalDataSource

    @Binds
    fun bindRemoteDataSource(dataSource: HotelsRemoteDataSourceImpl): HotelsRemoteDataSource

    @Binds
    fun bindRepository(repository: HotelsRepositoryImpl): HotelsRepository
}