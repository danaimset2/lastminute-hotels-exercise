package com.lastminute.exercise.hotels.data.local.di

import com.lastminute.exercise.hotels.data.local.database.HotelsDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class DaoModule {

    @Provides
    fun provideLocationDao(roomDb: HotelsDatabase) = roomDb.locationDao()

    @Provides
    fun provideContactDao(roomDb: HotelsDatabase) = roomDb.contactDao()

    @Provides
    fun provideTimeRangeDao(roomDb: HotelsDatabase) = roomDb.timeRangeDao()

    @Provides
    fun providePhotoDao(roomDb: HotelsDatabase) = roomDb.photoDao()

    @Provides
    fun provideHotelDao(roomDb: HotelsDatabase) = roomDb.hotelDao()

    @Provides
    fun provideFlightDao(roomDb: HotelsDatabase) = roomDb.flightDao()
}