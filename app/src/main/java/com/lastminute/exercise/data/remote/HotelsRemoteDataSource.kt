package com.lastminute.exercise.data.remote

import com.lastminute.exercise.data.models.HotelsWithHolidayPackages
import javax.inject.Inject

interface HotelsRemoteDataSource {

    suspend fun fetchHotelsWithHolidayPackages(): HotelsWithHolidayPackages
}

class HotelsRemoteDataSourceImpl @Inject constructor(
    private val api: Api
) : HotelsRemoteDataSource {

    override suspend fun fetchHotelsWithHolidayPackages(): HotelsWithHolidayPackages {
        return api.fetchHotelsWithHolidayPackages()
    }
}