package com.lastminute.exercise.hotels.data.local.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import com.lastminute.exercise.hotels.data.local.database.entities.FlightEntity

@Dao
interface FlightDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(flights: FlightEntity): Long
}