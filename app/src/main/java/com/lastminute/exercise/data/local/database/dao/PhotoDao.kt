package com.lastminute.exercise.hotels.data.local.database.dao

import androidx.room.Dao
import androidx.room.Insert
import com.lastminute.exercise.hotels.data.local.database.entities.PhotoEntity

@Dao
interface PhotoDao {

    @Insert
    suspend fun insertAll(gallery: List<PhotoEntity>)
}