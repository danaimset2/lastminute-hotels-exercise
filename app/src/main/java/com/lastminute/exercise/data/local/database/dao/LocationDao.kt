package com.lastminute.exercise.hotels.data.local.database.dao

import androidx.room.Dao
import androidx.room.Insert
import com.lastminute.exercise.hotels.data.local.database.entities.LocationEntity

@Dao
interface LocationDao {

    @Insert
    suspend fun insertAll(locations: List<LocationEntity>)
}