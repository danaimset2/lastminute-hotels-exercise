package com.lastminute.exercise.hotels.data.local.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import com.lastminute.exercise.hotels.data.local.HOTEL_TABLE_NAME
import com.lastminute.exercise.hotels.data.local.database.entities.HotelEntity
import com.lastminute.exercise.hotels.data.local.database.models.HotelWithRelations

@Dao
interface HotelDao {

    @Insert
    suspend fun insertAll(hotels: List<HotelEntity>)

    @Transaction
    @Query("select * from $HOTEL_TABLE_NAME where flight_id is null")
    suspend fun queryAll(): List<HotelWithRelations>

    @Transaction
    @Query("select * from $HOTEL_TABLE_NAME where id=:id")
    suspend fun queryById(id: Long): HotelWithRelations

    @Query("delete from $HOTEL_TABLE_NAME")
    suspend fun clearAll()

    @Query("select * from $HOTEL_TABLE_NAME where flight_id is not null")
    suspend fun queryHotelsWithFlight(): List<HotelWithRelations>
}