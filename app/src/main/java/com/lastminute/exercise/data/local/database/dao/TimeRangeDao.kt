package com.lastminute.exercise.hotels.data.local.database.dao

import androidx.room.Dao
import androidx.room.Insert
import com.lastminute.exercise.hotels.data.local.database.entities.TimeRangeEntity

@Dao
interface TimeRangeDao {

    @Insert
    suspend fun insertAll(timeRanges: List<TimeRangeEntity>)
}