package com.lastminute.exercise.hotels.data.local.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.lastminute.exercise.hotels.data.local.COLUMN_HOTEL_ID
import com.lastminute.exercise.hotels.data.local.COLUMN_ID
import com.lastminute.exercise.hotels.data.local.TIME_RANGE_TABLE_NAME

@Entity(
    tableName = TIME_RANGE_TABLE_NAME,
    foreignKeys = [ForeignKey(
        entity = HotelEntity::class,
        parentColumns = [COLUMN_ID],
        childColumns = [COLUMN_HOTEL_ID],
        onDelete = ForeignKey.CASCADE
    )]
)
data class TimeRangeEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = COLUMN_ID)
    val id: Long,
    @ColumnInfo(name = COLUMN_HOTEL_ID, index = true)
    val hotelId: Long,
    val from: String,
    val to: String,
    val type: TimeRangeType
)

enum class TimeRangeType {
    CHECK_IN,
    CHECK_OUT
}
