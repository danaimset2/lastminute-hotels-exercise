package com.lastminute.exercise.data.remote.di

import com.lastminute.exercise.data.remote.Api
import com.lastminute.exercise.data.remote.moshi.adapters.BigDecimalAdapter
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

@Module
@InstallIn(SingletonComponent::class)
class ApiModule {

    @Provides
    fun provideMoshi(): Moshi {
        return Moshi.Builder()
            .add(BigDecimalAdapter)
            .build()
    }

    @Provides
    fun provideJsonConverterFactory(moshi: Moshi): Converter.Factory {
        return MoshiConverterFactory.create(moshi)
    }

    @Provides
    fun provideRetrofit(converterFactory: Converter.Factory): Retrofit {
        return Retrofit.Builder()
            .baseUrl(API_BASE_URL)
            .addConverterFactory(converterFactory)
            .build()
    }

    @Provides
    fun provideApi(retrofit: Retrofit): Api {
        return retrofit.create(Api::class.java)
    }

    companion object {
        private const val API_BASE_URL = "https://run.mocky.io/"
    }
}