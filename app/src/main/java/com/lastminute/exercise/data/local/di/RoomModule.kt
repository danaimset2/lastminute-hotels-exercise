package com.lastminute.exercise.hotels.data.local.di

import android.content.Context
import androidx.room.Room
import com.lastminute.exercise.hotels.data.local.database.HotelsDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class RoomModule {

    @Provides
    fun provideHotelsDatabase(@ApplicationContext context: Context): HotelsDatabase {
        return Room.databaseBuilder(context, HotelsDatabase::class.java, DATABASE_NAME)
            .build()
    }

    companion object {
        private const val DATABASE_NAME = "hotels"
    }
}