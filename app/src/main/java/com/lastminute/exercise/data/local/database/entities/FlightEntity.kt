package com.lastminute.exercise.hotels.data.local.database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.lastminute.exercise.hotels.data.local.FLIGHT_TABLE_NAME

@Entity(tableName = FLIGHT_TABLE_NAME)
data class FlightEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Long,
    val airline: String,
    val arrivalAirport: String,
    val arrivalTime: String,
    val departureAirport: String,
    val departureTime: String
)
