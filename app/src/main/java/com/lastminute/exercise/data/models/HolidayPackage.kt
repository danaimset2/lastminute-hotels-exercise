package com.lastminute.exercise.data.models

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class HolidayPackage(
    val flight: Flight,
    val hotel: Hotel
)
