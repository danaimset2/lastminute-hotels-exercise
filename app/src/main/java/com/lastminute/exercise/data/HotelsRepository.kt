package com.lastminute.exercise.data

import com.lastminute.exercise.hotels.data.local.HotelsLocalDataSource
import com.lastminute.exercise.data.models.HolidayPackage
import com.lastminute.exercise.data.models.Hotel
import com.lastminute.exercise.data.remote.HotelsRemoteDataSource
import com.lastminute.exercise.utils.wrapEspressoIdlingResource
import javax.inject.Inject

interface HotelsRepository {

    suspend fun loadHotels(forceRefresh: Boolean): List<Hotel>

    suspend fun loadHolidayPackages(): List<HolidayPackage>

    suspend fun loadHotel(id: Long): Hotel
}

class HotelsRepositoryImpl @Inject constructor(
    private val remoteDataSource: HotelsRemoteDataSource,
    private val localDataSource: HotelsLocalDataSource
) : HotelsRepository {
    override suspend fun loadHotels(forceRefresh: Boolean): List<Hotel> {
        wrapEspressoIdlingResource {
            var hotels = localDataSource.loadHotels()
            if (forceRefresh || hotels.isNullOrEmpty()) {
                localDataSource.clearAll()
                val hotelsWithHolidayPackages = remoteDataSource.fetchHotelsWithHolidayPackages()
                hotels = hotelsWithHolidayPackages.hotels
                localDataSource.saveHotels(hotels)
                localDataSource.saveHolidayPackages(hotelsWithHolidayPackages.holidayPackages)
            }

            return hotels
        }
    }

    override suspend fun loadHolidayPackages(): List<HolidayPackage> {
        wrapEspressoIdlingResource {
            return localDataSource.loadHolidayPackages()
        }
    }

    override suspend fun loadHotel(id: Long): Hotel {
        wrapEspressoIdlingResource {
            return localDataSource.loadHotel(id)
        }
    }
}