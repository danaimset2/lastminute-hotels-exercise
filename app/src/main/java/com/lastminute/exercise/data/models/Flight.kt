package com.lastminute.exercise.data.models

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Flight(
    val airline: String,
    val arrivalAirport: String,
    val arrivalTime: String,
    val departureAirport: String,
    val departureTime: String
)
