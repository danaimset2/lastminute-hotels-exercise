package com.lastminute.exercise.hotels.data.local

const val LOCATION_TABLE_NAME = "location"
const val CONTACT_TABLE_NAME = "contact"
const val TIME_RANGE_TABLE_NAME = "time_range"
const val PHOTO_TABLE_NAME = "photo"
const val HOTEL_TABLE_NAME = "hotel"
const val FLIGHT_TABLE_NAME = "flight"

const val COLUMN_ID = "id"
const val COLUMN_HOTEL_ID = "hotel_id"
const val COLUMN_FLIGHT_ID = "flight_id"