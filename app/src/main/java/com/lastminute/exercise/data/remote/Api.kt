package com.lastminute.exercise.data.remote

import com.lastminute.exercise.data.models.HotelsWithHolidayPackages
import retrofit2.http.GET

interface Api {

    @GET(value = "v3/15ba244a-b200-4c57-ae65-4370b2af65fb")
    suspend fun fetchHotelsWithHolidayPackages(): HotelsWithHolidayPackages
}