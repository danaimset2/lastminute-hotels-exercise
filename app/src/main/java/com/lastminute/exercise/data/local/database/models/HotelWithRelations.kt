package com.lastminute.exercise.hotels.data.local.database.models

import androidx.room.Embedded
import androidx.room.Relation
import com.lastminute.exercise.hotels.data.local.COLUMN_FLIGHT_ID
import com.lastminute.exercise.hotels.data.local.COLUMN_HOTEL_ID
import com.lastminute.exercise.hotels.data.local.COLUMN_ID
import com.lastminute.exercise.hotels.data.local.database.entities.ContactEntity
import com.lastminute.exercise.hotels.data.local.database.entities.FlightEntity
import com.lastminute.exercise.hotels.data.local.database.entities.HotelEntity
import com.lastminute.exercise.hotels.data.local.database.entities.LocationEntity
import com.lastminute.exercise.hotels.data.local.database.entities.PhotoEntity
import com.lastminute.exercise.hotels.data.local.database.entities.TimeRangeEntity

data class HotelWithRelations(
    @Embedded
    val hotel: HotelEntity,
    @Relation(entityColumn = COLUMN_HOTEL_ID, parentColumn = COLUMN_ID)
    val location: LocationEntity,
    @Relation(entityColumn = COLUMN_HOTEL_ID, parentColumn = COLUMN_ID)
    val timeRanges: List<TimeRangeEntity>,
    @Relation(entityColumn = COLUMN_HOTEL_ID, parentColumn = COLUMN_ID)
    val contact: ContactEntity,
    @Relation(entityColumn = COLUMN_HOTEL_ID, parentColumn = COLUMN_ID)
    val gallery: List<PhotoEntity>,
    @Relation(parentColumn = COLUMN_FLIGHT_ID, entityColumn = COLUMN_ID, entity = FlightEntity::class)
    val flight: FlightEntity?
)
