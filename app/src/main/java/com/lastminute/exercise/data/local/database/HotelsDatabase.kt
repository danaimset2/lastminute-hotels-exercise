package com.lastminute.exercise.hotels.data.local.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.lastminute.exercise.hotels.data.local.database.converters.BigDecimalConverter
import com.lastminute.exercise.hotels.data.local.database.dao.ContactDao
import com.lastminute.exercise.hotels.data.local.database.dao.FlightDao
import com.lastminute.exercise.hotels.data.local.database.dao.HotelDao
import com.lastminute.exercise.hotels.data.local.database.dao.LocationDao
import com.lastminute.exercise.hotels.data.local.database.dao.PhotoDao
import com.lastminute.exercise.hotels.data.local.database.dao.TimeRangeDao
import com.lastminute.exercise.hotels.data.local.database.entities.ContactEntity
import com.lastminute.exercise.hotels.data.local.database.entities.FlightEntity
import com.lastminute.exercise.hotels.data.local.database.entities.HotelEntity
import com.lastminute.exercise.hotels.data.local.database.entities.LocationEntity
import com.lastminute.exercise.hotels.data.local.database.entities.PhotoEntity
import com.lastminute.exercise.hotels.data.local.database.entities.TimeRangeEntity

@Database(
    entities = [
        LocationEntity::class,
        ContactEntity::class,
        TimeRangeEntity::class,
        PhotoEntity::class,
        HotelEntity::class,
        FlightEntity::class
    ],
    version = 1,
    exportSchema = false
)
@TypeConverters(BigDecimalConverter::class)
abstract class HotelsDatabase : RoomDatabase() {
    abstract fun locationDao(): LocationDao
    abstract fun contactDao(): ContactDao
    abstract fun timeRangeDao(): TimeRangeDao
    abstract fun photoDao(): PhotoDao
    abstract fun hotelDao(): HotelDao
    abstract fun flightDao(): FlightDao
}