package com.lastminute.exercise.hotels.data.local.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.lastminute.exercise.hotels.data.local.COLUMN_FLIGHT_ID
import com.lastminute.exercise.hotels.data.local.COLUMN_ID
import com.lastminute.exercise.hotels.data.local.HOTEL_TABLE_NAME
import com.lastminute.exercise.data.models.Currency
import java.math.BigDecimal

@Entity(tableName = HOTEL_TABLE_NAME)
data class HotelEntity(
    @PrimaryKey
    @ColumnInfo(name = COLUMN_ID)
    val id: Long,
    val name: String,
    val stars: Int,
    val userRating: Float,
    val price: BigDecimal,
    val currency: Currency,
    @ColumnInfo(name = COLUMN_FLIGHT_ID)
    val flightId: Long? = null
)
