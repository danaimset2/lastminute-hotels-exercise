package com.lastminute.exercise.hotels.data.local

import com.lastminute.exercise.data.models.HolidayPackage
import com.lastminute.exercise.data.models.Hotel
import com.lastminute.exercise.hotels.data.local.database.dao.ContactDao
import com.lastminute.exercise.hotels.data.local.database.dao.FlightDao
import com.lastminute.exercise.hotels.data.local.database.dao.HotelDao
import com.lastminute.exercise.hotels.data.local.database.dao.LocationDao
import com.lastminute.exercise.hotels.data.local.database.dao.PhotoDao
import com.lastminute.exercise.hotels.data.local.database.dao.TimeRangeDao
import com.lastminute.exercise.hotels.data.local.database.models.HotelWithRelations
import javax.inject.Inject

interface HotelsLocalDataSource {

    suspend fun saveHotels(hotels: List<Hotel>)

    suspend fun loadHotels(): List<Hotel>

    suspend fun saveHolidayPackages(holidayPackages: List<HolidayPackage>)

    suspend fun loadHolidayPackages(): List<HolidayPackage>

    suspend fun loadHotel(id: Long): Hotel

    suspend fun clearAll()
}

class HotelsLocalDataSourceImpl @Inject constructor(
    private val contactDao: ContactDao,
    private val locationDao: LocationDao,
    private val timeRangeDao: TimeRangeDao,
    private val photoDao: PhotoDao,
    private val hotelDao: HotelDao,
    private val flightDao: FlightDao
) : HotelsLocalDataSource {

    override suspend fun saveHotels(hotels: List<Hotel>) {
        hotelDao.insertAll(hotels.map(Hotel::toEntity))
        contactDao.insertAll(hotels.map(Hotel::contactEntity))
        locationDao.insertAll(hotels.map(Hotel::locationEntity))
        timeRangeDao.insertAll(hotels.map(Hotel::timeRanges).flatten())
        photoDao.insertAll(hotels.map(Hotel::photos).flatten())
    }

    override suspend fun loadHotels(): List<Hotel> {
        return hotelDao.queryAll().map(HotelWithRelations::toHotel)
    }

    override suspend fun saveHolidayPackages(holidayPackages: List<HolidayPackage>) {
        val hotelAndFlightPairs = holidayPackages.map {
            val flightId = flightDao.insert(it.flight.toEntity())
            Pair(it.hotel, flightId)
        }
        val hotels = hotelAndFlightPairs.map { it.first }
        hotelDao.insertAll(hotelAndFlightPairs.map { it.first.toEntity(it.second) })
        contactDao.insertAll(hotels.map(Hotel::contactEntity))
        locationDao.insertAll(hotels.map(Hotel::locationEntity))
        timeRangeDao.insertAll(hotels.map(Hotel::timeRanges).flatten())
        photoDao.insertAll(hotels.map(Hotel::photos).flatten())
    }

    override suspend fun loadHolidayPackages(): List<HolidayPackage> {
        return hotelDao.queryHotelsWithFlight().map(HotelWithRelations::toHolidayPackage)
    }

    override suspend fun loadHotel(id: Long): Hotel {
        return hotelDao.queryById(id).toHotel()
    }

    override suspend fun clearAll() {
        hotelDao.clearAll()
    }
}