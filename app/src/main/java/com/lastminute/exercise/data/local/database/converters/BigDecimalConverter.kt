package com.lastminute.exercise.hotels.data.local.database.converters

import androidx.room.TypeConverter
import java.math.BigDecimal

object BigDecimalConverter {

    @JvmStatic
    @TypeConverter
    fun from(string: String) = BigDecimal(string)

    @JvmStatic
    @TypeConverter
    fun to(value: BigDecimal) = value.toString()
}