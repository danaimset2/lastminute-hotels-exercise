package com.lastminute.exercise.data.models

import java.math.BigDecimal

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Hotel(
    val id: Long,
    val name: String,
    val location: Location,
    val stars: Int,
    val checkIn: TimeRange,
    val checkOut: TimeRange,
    val contact: Contact,
    val gallery: List<String>,
    val userRating: Float,
    val price: BigDecimal,
    val currency: Currency
)
