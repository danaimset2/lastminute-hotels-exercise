package com.lastminute.exercise.hotels.data.local

import com.lastminute.exercise.hotels.data.local.database.entities.ContactEntity
import com.lastminute.exercise.hotels.data.local.database.entities.FlightEntity
import com.lastminute.exercise.hotels.data.local.database.entities.HotelEntity
import com.lastminute.exercise.hotels.data.local.database.entities.LocationEntity
import com.lastminute.exercise.hotels.data.local.database.entities.PhotoEntity
import com.lastminute.exercise.hotels.data.local.database.entities.TimeRangeEntity
import com.lastminute.exercise.hotels.data.local.database.entities.TimeRangeType
import com.lastminute.exercise.hotels.data.local.database.models.HotelWithRelations
import com.lastminute.exercise.data.models.Contact
import com.lastminute.exercise.data.models.Flight
import com.lastminute.exercise.data.models.HolidayPackage
import com.lastminute.exercise.data.models.Hotel
import com.lastminute.exercise.data.models.Location
import com.lastminute.exercise.data.models.TimeRange

fun Hotel.toEntity(flightId: Long? = null) = HotelEntity(
    id,
    name,
    stars,
    userRating,
    price,
    currency,
    flightId
)

fun Hotel.contactEntity() = ContactEntity(
    0, // autogenerated
    id,
    contact.phoneNumber,
    contact.email
)

fun Hotel.locationEntity() = LocationEntity(
    0, // autogenerated
    id,
    location.address,
    location.city,
    location.latitude,
    location.longitude
)

fun Hotel.timeRanges() = listOf(
    TimeRangeEntity(
        0, // autogenerated
        id,
        checkIn.from,
        checkIn.to,
        TimeRangeType.CHECK_IN
    ),
    TimeRangeEntity(
        0, // autogenerated
        id,
        checkOut.from,
        checkOut.to,
        TimeRangeType.CHECK_OUT
    )
)

fun Hotel.photos() = gallery.map {
    PhotoEntity(
        0, // autogenerated
        id,
        it
    )
}

fun HotelWithRelations.toHotel() = Hotel(
    hotel.id,
    hotel.name,
    Location(
        location.address,
        location.city,
        location.latitude,
        location.longitude
    ),
    hotel.stars,
    checkIn = with(timeRanges.first { it.type == TimeRangeType.CHECK_IN }) {
        TimeRange(from, to)
    },
    checkOut = with(timeRanges.first { it.type == TimeRangeType.CHECK_OUT }) {
        TimeRange(from, to)
    },
    Contact(
        contact.phoneNumber,
        contact.email
    ),
    gallery.map { it.url },
    hotel.userRating,
    hotel.price,
    hotel.currency
)

fun Flight.toEntity() = FlightEntity(
    0, // autogenerated
    airline,
    arrivalAirport,
    arrivalTime,
    departureAirport,
    departureTime
)

fun FlightEntity.flight() = Flight(
    airline, arrivalAirport, arrivalTime, departureAirport, departureTime
)

fun HotelWithRelations.toHolidayPackage() = HolidayPackage(
    requireNotNull(flight).flight(),
    toHotel()
)