package com.lastminute.exercise.splash

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lastminute.exercise.hotels.data.local.HotelsLocalDataSource
import com.lastminute.exercise.utils.SingleLiveEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SplashViewModel @Inject constructor(
    private val hotelsLocalDataSource: HotelsLocalDataSource
) : ViewModel() {

    private val _startUpEvent: SingleLiveEvent<Unit> = SingleLiveEvent()
    val startUpEvent: SingleLiveEvent<Unit> = _startUpEvent

    fun cleanUp() {
        viewModelScope.launch {
            hotelsLocalDataSource.clearAll()
            _startUpEvent.value = Unit
        }
    }
}