package com.lastminute.exercise.splash

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.lastminute.exercise.HotelsActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SplashActivity : AppCompatActivity() {

    private val viewModel: SplashViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.startUpEvent.observe(this) {
            val hotelsIntent = Intent(this, HotelsActivity::class.java)
            startActivity(hotelsIntent)
            finish()
        }
        viewModel.cleanUp()
    }
}