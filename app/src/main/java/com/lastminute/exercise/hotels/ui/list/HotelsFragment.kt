package com.lastminute.exercise.hotels.ui.list

import android.os.Bundle
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.recyclerview.widget.DividerItemDecoration
import com.lastminute.exercise.R
import com.lastminute.exercise.data.models.Hotel
import com.lastminute.exercise.databinding.FragmentHotelsBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HotelsFragment : Fragment(R.layout.fragment_hotels) {

    private val hotelsViewModel: HotelsViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        FragmentHotelsBinding.bind(view).apply {
            NavigationUI.setupWithNavController(toolbar, findNavController())
            setupSwipeToRefresh()
            setupHotelsRecycler()
        }
        hotelsViewModel.loadHotels()
    }

    private fun FragmentHotelsBinding.setupSwipeToRefresh() {
        hotelsSwipeRefresh.setOnRefreshListener {
            hotelsViewModel.loadHotels(true)
        }
        hotelsViewModel.loading.observe(viewLifecycleOwner) {
            hotelsSwipeRefresh.isRefreshing = it
        }
    }

    private fun FragmentHotelsBinding.setupHotelsRecycler() {
        val hotelsAdapter = HotelsAdapter {
            val direction = HotelsFragmentDirections
                .actionHotelsFragmentToHotelDetailsFragment(it)
            findNavController().navigate(direction)
        }
        hotelsRecycler.adapter = hotelsAdapter
        val dividerItemDecoration = DividerItemDecoration(root.context, DividerItemDecoration.VERTICAL)
        val dividerDrawable = AppCompatResources.getDrawable(root.context, R.drawable.divider)
        dividerDrawable?.let { dividerItemDecoration.setDrawable(it) }
        hotelsRecycler.addItemDecoration(dividerItemDecoration)
        hotelsViewModel.hotels.observe(viewLifecycleOwner) {
            hotelsAdapter.submitList(it.map(Hotel::toItem))
        }
    }
}
