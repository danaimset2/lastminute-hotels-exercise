package com.lastminute.exercise.hotels.ui.list

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lastminute.exercise.data.models.Hotel
import com.lastminute.exercise.domain.LoadHotelsUseCase
import com.lastminute.exercise.domain.Result
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HotelsViewModel @Inject constructor(
    private val loadHotelsUseCase: LoadHotelsUseCase
) : ViewModel() {

    private val _loading: MutableLiveData<Boolean> = MutableLiveData()
    val loading: LiveData<Boolean> = _loading

    private val _hotels: MutableLiveData<List<Hotel>> = MutableLiveData()
    val hotels: LiveData<List<Hotel>> = _hotels

    fun loadHotels(reload: Boolean = false) {
        _loading.value = true
        viewModelScope.launch {
            when (val result = loadHotelsUseCase.execute(reload)) {
                is Result.Success -> _hotels.value = result.data
                is Result.Error -> {
                    Log.e("TAG", result.exception.message.orEmpty())
                    _hotels.value = emptyList()
                }
            }
            _loading.value = false
        }
    }
}