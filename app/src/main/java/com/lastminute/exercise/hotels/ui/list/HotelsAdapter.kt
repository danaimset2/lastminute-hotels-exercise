package com.lastminute.exercise.hotels.ui.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil.ItemCallback
import androidx.recyclerview.widget.ListAdapter
import com.lastminute.exercise.databinding.ItemHotelBinding

class HotelsAdapter(
    private val onItemClick: (Long) -> Unit
) : ListAdapter<HotelItem, HotelItemViewHolder>(DiffItemCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HotelItemViewHolder {
        val binding = ItemHotelBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return HotelItemViewHolder(binding).apply {
            this.itemView.setOnClickListener {
                onItemClick.invoke(getItem(adapterPosition).id)
            }
        }
    }

    override fun onBindViewHolder(holder: HotelItemViewHolder, position: Int) {
        holder.bindHotel(getItem(position))
    }

    companion object DiffItemCallback : ItemCallback<HotelItem>() {
        override fun areItemsTheSame(oldItem: HotelItem, newItem: HotelItem): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: HotelItem, newItem: HotelItem): Boolean {
            return oldItem == newItem
        }
    }
}