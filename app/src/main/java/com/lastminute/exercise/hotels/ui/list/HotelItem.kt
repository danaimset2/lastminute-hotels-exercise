package com.lastminute.exercise.hotels.ui.list

import com.lastminute.exercise.data.models.Currency
import com.lastminute.exercise.data.models.Hotel

data class HotelItem(
    val id: Long,
    val name: String,
    val price: String,
    val address: String,
    val city: String,
    val stars: Int,
    val userRating: String,
    val coverUrl: String
)

fun Currency.toCode() = when (this) {
    Currency.EUR -> "€"
}

fun Hotel.toItem() = HotelItem(
    id,
    name,
    "${currency.toCode()} $price",
    location.address,
    location.city,
    stars,
    userRating.toString(),
    gallery.firstOrNull().orEmpty()
)
