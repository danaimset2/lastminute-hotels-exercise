package com.lastminute.exercise.hotels.ui.details

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import com.lastminute.exercise.data.models.Hotel
import com.lastminute.exercise.domain.LoadHotelDetailsUseCase
import com.lastminute.exercise.domain.Result
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HotelDetailsViewModel @Inject constructor(
    private val loadHotelDetailsUseCase: LoadHotelDetailsUseCase
) : ViewModel() {

    private val _hotel: MutableLiveData<Hotel> = MutableLiveData()
    val hotel = _hotel.map { it }

    fun loadHotel(id: Long) {
        viewModelScope.launch {
            when (val result = loadHotelDetailsUseCase.execute(id)) {
                is Result.Success -> _hotel.value = result.data
                is Result.Error -> {
                    // TODO: Handle hotel not found exception etc.
                }
            }
        }
    }
}