package com.lastminute.exercise.hotels.ui.details

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.lastminute.exercise.R
import com.lastminute.exercise.databinding.FragmentHotelDetailsBinding
import com.lastminute.exercise.hotels.ui.list.toCode
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HotelDetailsFragment : Fragment(R.layout.fragment_hotel_details) {

    private val hotelId by lazy {
        HotelDetailsFragmentArgs.fromBundle(requireArguments()).hotelId
    }
    private val hotelDetailsViewModel: HotelDetailsViewModel by viewModels()

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        FragmentHotelDetailsBinding.bind(view).apply {
            setupToolbar()
            hotelDetailsViewModel.hotel.observe(viewLifecycleOwner) { hotel ->
                collapsingToolbar.title = hotel.name
                price.text = "${hotel.currency.toCode()} ${hotel.price}"
                userRating.text = "(${hotel.userRating.toString()})"
                ratingBar.rating = hotel.stars.toFloat()
                checkInFrom.text = hotel.checkIn.from
                checkInTo.text = hotel.checkIn.to
                checkOutFrom.text = hotel.checkOut.from
                checkOutTo.text = hotel.checkOut.to
                address.text = hotel.location.address
                city.text = hotel.location.city
                email.text = hotel.contact.email
                phone.text = hotel.contact.phoneNumber
                hotel.gallery.firstOrNull()?.let {
                    Glide.with(view)
                        .load(it)
                        .transition(withCrossFade())
                        .into(cover)
                }
            }
        }

        hotelDetailsViewModel.loadHotel(hotelId)
    }

    fun FragmentHotelDetailsBinding.setupToolbar() {
        toolbar.setNavigationIcon(R.drawable.ic_back)
        toolbar.setNavigationOnClickListener {
            findNavController().popBackStack()
        }
    }
}
