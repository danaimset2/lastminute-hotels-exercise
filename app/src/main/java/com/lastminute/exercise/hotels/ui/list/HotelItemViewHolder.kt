package com.lastminute.exercise.hotels.ui.list

import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.lastminute.exercise.databinding.ItemHotelBinding

class HotelItemViewHolder(private val binding: ItemHotelBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bindHotel(hotel: HotelItem) {
        with(binding) {
            name.text = hotel.name
            price.text = hotel.price
            city.text = hotel.city
            address.text = hotel.address
            ratingBar.rating = hotel.stars.toFloat()
            userRating.text = hotel.userRating
            Glide.with(binding.root)
                .load(hotel.coverUrl)
                .transition(withCrossFade())
                .into(cover)
        }
    }
}