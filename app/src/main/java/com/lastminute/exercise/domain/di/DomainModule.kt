package com.lastminute.exercise.domain.di

import com.lastminute.exercise.domain.LoadHolidaysUseCase
import com.lastminute.exercise.domain.LoadHolidaysUseCaseImpl
import com.lastminute.exercise.domain.LoadHotelDetailsUseCase
import com.lastminute.exercise.domain.LoadHotelDetailsUseCaseImpl
import com.lastminute.exercise.domain.LoadHotelsUseCase
import com.lastminute.exercise.domain.LoadHotelsUseCaseImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface DomainModule {

    @Binds
    fun bindLoadHotelsUseCase(useCase: LoadHotelsUseCaseImpl): LoadHotelsUseCase

    @Binds
    fun bindLoadHolidaysUseCase(useCase: LoadHolidaysUseCaseImpl): LoadHolidaysUseCase

    @Binds
    fun bindLoadHotelDetailsUseCase(useCase: LoadHotelDetailsUseCaseImpl): LoadHotelDetailsUseCase
}