package com.lastminute.exercise.domain

import com.lastminute.exercise.data.HotelsRepository
import com.lastminute.exercise.data.models.Hotel
import javax.inject.Inject

interface LoadHotelsUseCase {

    suspend fun execute(reload: Boolean): Result<List<Hotel>>
}

class LoadHotelsUseCaseImpl @Inject constructor(
    private val hotelsRepository: HotelsRepository
) : LoadHotelsUseCase {

    override suspend fun execute(reload: Boolean): Result<List<Hotel>> {
        return try {
            val hotels = hotelsRepository.loadHotels(reload)
            Result.Success(hotels)
        } catch (e: Exception) {
            Result.Error(e)
        }
    }
}