package com.lastminute.exercise.domain

import com.lastminute.exercise.data.HotelsRepository
import com.lastminute.exercise.data.models.HolidayPackage
import javax.inject.Inject

interface LoadHolidaysUseCase {

    suspend fun execute(reload: Boolean): Result<List<HolidayPackage>>
}

class LoadHolidaysUseCaseImpl @Inject constructor(
    private val hotelsRepository: HotelsRepository
) : LoadHolidaysUseCase {

    override suspend fun execute(reload: Boolean): Result<List<HolidayPackage>> {
        return try {
            val holidays = hotelsRepository.loadHolidayPackages()
            Result.Success(holidays)
        } catch (e: Exception) {
            Result.Error(e)
        }
    }
}