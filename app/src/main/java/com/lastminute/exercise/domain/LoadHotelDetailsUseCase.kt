package com.lastminute.exercise.domain

import com.lastminute.exercise.data.HotelsRepository
import com.lastminute.exercise.data.models.Hotel
import javax.inject.Inject

interface LoadHotelDetailsUseCase {

    suspend fun execute(id: Long): Result<Hotel>
}

class LoadHotelDetailsUseCaseImpl @Inject constructor(
    private val hotelsRepository: HotelsRepository
) : LoadHotelDetailsUseCase {

    override suspend fun execute(id: Long): Result<Hotel> {
        return Result.Success(hotelsRepository.loadHotel(id))
    }
}