package com.lastminute.exercise

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import com.lastminute.exercise.R
import com.lastminute.exercise.databinding.ActivityHotelsBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HotelsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHotelsBinding
    private val navController by lazy {
        supportFragmentManager.findFragmentById(R.id.navHostFragment)!!.findNavController()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHotelsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        NavigationUI.setupWithNavController(binding.bottomNavigation, navController)
    }
}