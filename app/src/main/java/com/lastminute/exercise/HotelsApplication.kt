package com.lastminute.exercise

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class HotelsApplication : Application()