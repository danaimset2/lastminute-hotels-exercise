package com.lastminute.exercise.holiday

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lastminute.exercise.data.models.HolidayPackage
import com.lastminute.exercise.domain.LoadHolidaysUseCase
import com.lastminute.exercise.domain.Result
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HolidaysViewModel @Inject constructor(
    private val loadHolidaysUseCase: LoadHolidaysUseCase
) : ViewModel() {

    private val _loading: MutableLiveData<Boolean> = MutableLiveData()
    val loading: LiveData<Boolean> = _loading

    private val _holidays: MutableLiveData<List<HolidayPackage>> = MutableLiveData()
    val hotels: LiveData<List<HolidayPackage>> = _holidays

    fun loadHolidays(reload: Boolean = false) {
        _loading.value = true
        viewModelScope.launch {
            when (val result = loadHolidaysUseCase.execute(reload)) {
                is Result.Success -> _holidays.value = result.data
                is Result.Error -> {
                    Log.e("TAG", result.exception.message.orEmpty())
                    _holidays.value = emptyList()
                }
            }
            _loading.value = false
        }
    }
}