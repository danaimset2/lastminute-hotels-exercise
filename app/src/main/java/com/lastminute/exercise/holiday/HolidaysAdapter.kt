package com.lastminute.exercise.holiday

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil.ItemCallback
import androidx.recyclerview.widget.ListAdapter
import com.lastminute.exercise.databinding.ItemHolidayBinding

class HolidaysAdapter(
    private val onItemClick: (Long) -> Unit
) : ListAdapter<HolidayItem, HolidayItemViewHolder>(DiffItemCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolidayItemViewHolder {
        val binding = ItemHolidayBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return HolidayItemViewHolder(binding).apply {
            binding.hotelCard.setOnClickListener {
                onItemClick.invoke(getItem(adapterPosition).hotel.id)
            }
        }
    }

    override fun onBindViewHolder(holder: HolidayItemViewHolder, position: Int) {
        holder.bindHoliday(getItem(position))
    }

    companion object DiffItemCallback : ItemCallback<HolidayItem>() {
        override fun areItemsTheSame(oldItem: HolidayItem, newItem: HolidayItem): Boolean {
            return oldItem.hotel.id == newItem.hotel.id
        }

        override fun areContentsTheSame(oldItem: HolidayItem, newItem: HolidayItem): Boolean {
            return oldItem == newItem
        }
    }
}