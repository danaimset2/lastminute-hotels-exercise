package com.lastminute.exercise.holiday

import com.lastminute.exercise.data.models.Currency
import com.lastminute.exercise.data.models.Flight
import com.lastminute.exercise.data.models.HolidayPackage
import com.lastminute.exercise.hotels.ui.list.HotelItem
import com.lastminute.exercise.hotels.ui.list.toItem

data class FlightItem(
    val info: String
)

fun Flight.toItem() = FlightItem(
    "${this.departureAirport} - ${this.arrivalAirport}"
)

data class HolidayItem(
    val hotel: HotelItem,
    val flight: FlightItem
)

fun Currency.toCode() = when (this) {
    Currency.EUR -> "€"
}

fun HolidayPackage.toItem() = HolidayItem(
    hotel.toItem(),
    flight.toItem()
)
