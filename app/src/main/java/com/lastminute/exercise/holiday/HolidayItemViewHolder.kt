package com.lastminute.exercise.holiday

import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.lastminute.exercise.databinding.ItemHolidayBinding

class HolidayItemViewHolder(private val binding: ItemHolidayBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bindHoliday(holiday: HolidayItem) {
        with(binding) {
            val hotel = holiday.hotel
            name.text = hotel.name
            price.text = hotel.price
            city.text = hotel.city
            address.text = hotel.address
            ratingBar.rating = hotel.stars.toFloat()
            userRating.text = hotel.userRating
            Glide.with(binding.root)
                .load(hotel.coverUrl)
                .transition(withCrossFade())
                .into(cover)
            flightInfo.text = holiday.flight.info
        }
    }
}