package com.lastminute.exercise.holiday

import android.os.Bundle
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.recyclerview.widget.DividerItemDecoration
import com.lastminute.exercise.R
import com.lastminute.exercise.data.models.HolidayPackage
import com.lastminute.exercise.databinding.FragmentHolidaysBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HolidaysFragment : Fragment(R.layout.fragment_holidays) {

    private val holidaysViewModel: HolidaysViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        FragmentHolidaysBinding.bind(view).apply {
            NavigationUI.setupWithNavController(toolbar, findNavController())
            setupSwipeToRefresh()
            setupHolidaysRecycler()
        }
        holidaysViewModel.loadHolidays()
    }

    private fun FragmentHolidaysBinding.setupSwipeToRefresh() {
        holidaysSwipeRefresh.setOnRefreshListener {
            holidaysViewModel.loadHolidays(true)
        }
        holidaysViewModel.loading.observe(viewLifecycleOwner) {
            holidaysSwipeRefresh.isRefreshing = it
        }
    }

    private fun FragmentHolidaysBinding.setupHolidaysRecycler() {
        val holidaysAdapter = HolidaysAdapter {
            val direction = HolidaysFragmentDirections
                .actionHolidaysFragmentToHotelDetailsFragment(it)
            findNavController().navigate(direction)
        }
        holidaysRecycler.adapter = holidaysAdapter
        val dividerItemDecoration = DividerItemDecoration(root.context, DividerItemDecoration.VERTICAL)
        val dividerDrawable = AppCompatResources.getDrawable(root.context, R.drawable.divider)
        dividerDrawable?.let { dividerItemDecoration.setDrawable(it) }
        holidaysRecycler.addItemDecoration(dividerItemDecoration)
        holidaysViewModel.hotels.observe(viewLifecycleOwner) {
            holidaysAdapter.submitList(it.map(HolidayPackage::toItem))
        }
    }
}
