package com.lastminute.exercise

import com.lastminute.exercise.hotels.data.local.database.entities.ContactEntity
import com.lastminute.exercise.hotels.data.local.database.entities.HotelEntity
import com.lastminute.exercise.hotels.data.local.database.entities.LocationEntity
import com.lastminute.exercise.hotels.data.local.database.entities.PhotoEntity
import com.lastminute.exercise.hotels.data.local.database.entities.TimeRangeEntity
import com.lastminute.exercise.hotels.data.local.database.entities.TimeRangeType
import com.lastminute.exercise.hotels.data.local.database.models.HotelWithRelations
import com.lastminute.exercise.data.models.Contact
import com.lastminute.exercise.data.models.Currency
import com.lastminute.exercise.data.models.Hotel
import com.lastminute.exercise.data.models.Location
import com.lastminute.exercise.data.models.TimeRange
import java.math.BigDecimal

object TestUtils {

    fun generateHotel(
        id: Long = 1,
        name: String = "Cortez",
        location: Location = Location(
            "317 South Spring Street",
            "Los Angeles",
            34.0500096,
            -118.2496724
        ),
        stars: Int = 5,
        checkIn: TimeRange = TimeRange("00:00", "12:00"),
        checkOut: TimeRange = TimeRange("14:00", "20:00"),
        contact: Contact = Contact(
            "+14154006845",
            "danaimset@gmail.com"
        ),
        gallery: List<String> = listOf(
            "https://static.wikia.nocookie.net/americanhorrorstory/images/7/73/AHS_Hotel_Cortez_Lobby.jpeg/revision/latest/scale-to-width-down/1000?cb=20151108195516",
            "https://static.wikia.nocookie.net/americanhorrorstory/images/0/0b/Hotel_Cortez_Lobby_001.jpeg/revision/latest/scale-to-width-down/1000?cb=20151011140407",
            "https://static.wikia.nocookie.net/americanhorrorstory/images/0/07/Hotel_Cortez_Lobby_002.jpeg/revision/latest/scale-to-width-down/1000?cb=20151011140515"
        ),
        userRating: Float = 5f,
        price: BigDecimal = BigDecimal(100),
        currency: Currency = Currency.EUR
    ) = Hotel(
        id,
        name,
        location,
        stars,
        checkIn,
        checkOut,
        contact,
        gallery,
        userRating,
        price,
        currency
    )

    fun generateHotelEntity(id: Long) = HotelEntity(
        id,
        "Hotel",
        1,
        2.5f,
        BigDecimal(100),
        Currency.EUR
    )

    fun generateContactEntity(hotelId: Long) = ContactEntity(
        1,
        hotelId,
        "+333-333-33-33",
        "hotel@gmail.com"
    )

    fun generateLocationEntity(hotelId: Long) = LocationEntity(
        1,
        hotelId,
        "123 str",
        "Big city",
        50.0,
        50.0
    )

    fun generateTimeRangeEntity(hotelId: Long, type: TimeRangeType) = TimeRangeEntity(
        1,
        hotelId,
        "10:00",
        "12:00",
        type
    )

    fun generatePhotoEntity(hotelId: Long) = PhotoEntity(
        1,
        hotelId,
        "link"
    )

    fun generateHotelWithRelations(
        hotel: HotelEntity = generateHotelEntity(1),
        location: LocationEntity = generateLocationEntity(hotel.id),
        timeRanges: List<TimeRangeEntity> = listOf(
            generateTimeRangeEntity(hotel.id, TimeRangeType.CHECK_IN),
            generateTimeRangeEntity(hotel.id, TimeRangeType.CHECK_OUT)
        ),
        contact: ContactEntity = generateContactEntity(hotel.id),
        gallery: List<PhotoEntity> = listOf(
            generatePhotoEntity(hotel.id)
        )
    ) = HotelWithRelations(
        hotel,
        location,
        timeRanges,
        contact,
        gallery
    )
}