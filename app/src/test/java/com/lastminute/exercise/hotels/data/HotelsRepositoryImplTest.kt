package com.lastminute.exercise.hotels.data

import com.lastminute.exercise.TestUtils
import com.lastminute.exercise.data.HotelsRepositoryImpl
import com.lastminute.exercise.hotels.data.local.HotelsLocalDataSource
import com.lastminute.exercise.data.remote.HotelsRemoteDataSource
import com.lastminute.exercise.ext.deepEqualTo
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Test
import org.mockito.kotlin.inOrder
import org.mockito.kotlin.mock
import org.mockito.kotlin.never
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class HotelsRepositoryImplTest {
    private val remoteDataSource: HotelsRemoteDataSource = mock()
    private val localDataSource: HotelsLocalDataSource = mock()
    private val repository = HotelsRepositoryImpl(remoteDataSource, localDataSource)

    @Test
    fun `when local data is empty then fetch hotels from remote data source`() = runBlockingTest {
        val hotels = listOf(TestUtils.generateHotel(1, "Hotel 1"), TestUtils.generateHotel(2, "Hotel 2"))
        whenever(localDataSource.loadHotels()).thenReturn(emptyList())
        whenever(remoteDataSource.fetchHotelsWithHolidayPackages()).thenReturn(hotels)

        val loadedHotels = repository.loadHotels(false)
        Assert.assertTrue(hotels deepEqualTo loadedHotels)

        val inOrder = inOrder(
            localDataSource,
            localDataSource,
            remoteDataSource,
            localDataSource
        )
        inOrder.verify(localDataSource).loadHotels()
        inOrder.verify(localDataSource).clearAll()
        inOrder.verify(remoteDataSource).fetchHotelsWithHolidayPackages()
        inOrder.verify(localDataSource).saveHotels(hotels)
    }

    @Test
    fun `when local data is not empty but forceRefresh true then fetch hotels from remote`() = runBlockingTest {
        val remoteHotels = listOf(TestUtils.generateHotel(3, "Hotel 3"), TestUtils.generateHotel(4, "Hotel 4"))
        val localHotels = listOf(TestUtils.generateHotel(1, "Hotel 1"), TestUtils.generateHotel(2, "Hotel 2"))
        whenever(localDataSource.loadHotels()).thenReturn(localHotels)
        whenever(remoteDataSource.fetchHotelsWithHolidayPackages()).thenReturn(remoteHotels)

        val loadedHotels = repository.loadHotels(true)
        Assert.assertTrue(remoteHotels deepEqualTo loadedHotels)

        val inOrder = inOrder(
            localDataSource,
            localDataSource,
            remoteDataSource,
            localDataSource
        )
        inOrder.verify(localDataSource).loadHotels()
        inOrder.verify(localDataSource).clearAll()
        inOrder.verify(remoteDataSource).fetchHotelsWithHolidayPackages()
        inOrder.verify(localDataSource).saveHotels(remoteHotels)
    }

    @Test
    fun `when local data is not empty and forceRefresh is false the load hotels from local only`() = runBlockingTest {
        val remoteHotels = listOf(TestUtils.generateHotel(3, "Hotel 3"), TestUtils.generateHotel(4, "Hotel 4"))
        val localHotels = listOf(TestUtils.generateHotel(1, "Hotel 1"), TestUtils.generateHotel(2, "Hotel 2"))
        whenever(localDataSource.loadHotels()).thenReturn(localHotels)
        whenever(remoteDataSource.fetchHotelsWithHolidayPackages()).thenReturn(remoteHotels)

        val loadedHotels = repository.loadHotels(false)
        Assert.assertTrue(localHotels deepEqualTo loadedHotels)

        verify(localDataSource).loadHotels()
        verify(localDataSource, never()).clearAll()
        verify(remoteDataSource, never()).fetchHotelsWithHolidayPackages()
        verify(localDataSource, never()).saveHotels(remoteHotels)
    }

    @Test
    fun loadHotel() = runBlockingTest {
        val hotel = TestUtils.generateHotel()
        whenever(localDataSource.loadHotel(1)).thenReturn(hotel)

        val loadedHotel = repository.loadHotel(1)

        Assert.assertEquals(hotel, loadedHotel)

        verify(localDataSource).loadHotel(1)
    }
}
