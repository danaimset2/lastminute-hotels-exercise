package com.lastminute.exercise.hotels.data.remote

import com.lastminute.exercise.TestUtils
import com.lastminute.exercise.data.remote.Api
import com.lastminute.exercise.data.remote.HotelsRemoteDataSourceImpl
import com.lastminute.exercise.ext.deepEqualTo
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class HotelsRemoteDataSourceImplTest {
    
    private val api: Api = mock()
    private val dataSource = HotelsRemoteDataSourceImpl(api)

    @Test
    fun fetchHotels() = runBlockingTest {
        val hotels = listOf(
            TestUtils.generateHotel(id = 1, name = "Hotel 1"),
            TestUtils.generateHotel(id = 2, name = "Hotel 2")
        )
        whenever(api.fetchHotelsWithHolidayPackages()).thenReturn(hotels)

        val fetchedHotels = dataSource.fetchHotelsWithHolidayPackages()

        Assert.assertTrue(hotels deepEqualTo fetchedHotels)

        verify(api).fetchHotelsWithHolidayPackages()
    }
}