package com.lastminute.exercise.hotels.ui.details

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.lastminute.exercise.MainCoroutineRule
import com.lastminute.exercise.TestUtils
import com.lastminute.exercise.domain.LoadHotelDetailsUseCase
import com.lastminute.exercise.domain.Result
import com.lastminute.exercise.getOrAwaitValue
import com.lastminute.exercise.observeForTesting
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class HotelDetailsViewModelTest {

    private val useCase: LoadHotelDetailsUseCase = mock()

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @Test
    fun `when load success then emit hotel`() = mainCoroutineRule.runBlockingTest {
        val hotelDetailsViewModel = HotelDetailsViewModel(useCase)
        val hotel = TestUtils.generateHotel(1)

        whenever(useCase.execute(any()))
            .thenReturn(Result.Success(hotel))

        hotelDetailsViewModel.loadHotel(1)

        hotelDetailsViewModel.hotel.observeForTesting {
            Assert.assertEquals(hotel, hotelDetailsViewModel.hotel.getOrAwaitValue())
        }
        
        verify(useCase).execute(1)
    }

    @Test
    fun `when load error then notify error`() = mainCoroutineRule.runBlockingTest {
        // TODO: Test loading error behavior
    }
}