package com.lastminute.exercise.hotels.data.remote.moshi.adapters

import com.lastminute.exercise.data.remote.moshi.adapters.BigDecimalAdapter
import org.junit.Assert.assertEquals
import org.junit.Test
import java.math.BigDecimal

class BigDecimalAdapterTest {

    @Test
    fun fromJson() {
        val bigDecimalJson = "100.50"
        val expectedBigDecimal = BigDecimal("100.50")
        val actualBigDecimal = BigDecimalAdapter.fromJson(bigDecimalJson)

        assertEquals(expectedBigDecimal, actualBigDecimal)
    }

    @Test
    fun toJson() {
        val bigDecimal = BigDecimal("100")
        val expectedJson = "100"
        val actualJson = BigDecimalAdapter.toJson(bigDecimal)

        assertEquals(expectedJson, actualJson)
    }
}