package com.lastminute.exercise.hotels.ui.list

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.lastminute.exercise.MainCoroutineRule
import com.lastminute.exercise.TestUtils
import com.lastminute.exercise.domain.LoadHotelsUseCase
import com.lastminute.exercise.domain.Result
import com.lastminute.exercise.ext.deepEqualTo
import com.lastminute.exercise.getOrAwaitValue
import com.lastminute.exercise.observeForTesting
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class HotelsViewModelTest {

    private val fakeLoadHotelsUseCase: LoadHotelsUseCase = mock()
    private val hotelsViewModel = HotelsViewModel(fakeLoadHotelsUseCase)

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @Test
    fun `when load success then emit list of hotels`() = runBlockingTest {
        // Pause main dispatcher to assert initial value
        mainCoroutineRule.pauseDispatcher()

        val hotels = listOf(
            TestUtils.generateHotel(1),
            TestUtils.generateHotel(2)
        )

        whenever(fakeLoadHotelsUseCase.execute(any()))
            .thenReturn(Result.Success(hotels))

        hotelsViewModel.loadHotels()

        hotelsViewModel.hotels.observeForTesting {
            // Check initial value when loading hotels
            assertTrue(hotelsViewModel.loading.getOrAwaitValue())
            // Continue coroutine execution
            mainCoroutineRule.resumeDispatcher()

            assertTrue(hotels deepEqualTo hotelsViewModel.hotels.getOrAwaitValue())
            assertFalse(hotelsViewModel.loading.getOrAwaitValue())
        }

        verify(fakeLoadHotelsUseCase).execute(false)
    }

    @Test
    fun `when load error then emit empty list`() = runBlockingTest {
        // Pause main dispatcher to assert initial value
        mainCoroutineRule.pauseDispatcher()

        val exception = IllegalStateException("Error")
        whenever(fakeLoadHotelsUseCase.execute(any()))
            .thenReturn(Result.Error(exception))

        hotelsViewModel.loadHotels()

        hotelsViewModel.hotels.observeForTesting {
            // Check initial value when loading hotels
            assertTrue(hotelsViewModel.loading.getOrAwaitValue())
            // Continue coroutine execution
            mainCoroutineRule.resumeDispatcher()

            assertTrue(hotelsViewModel.hotels.getOrAwaitValue().isEmpty())
            assertFalse(hotelsViewModel.loading.getOrAwaitValue())
        }

        verify(fakeLoadHotelsUseCase).execute(false)
    }
}