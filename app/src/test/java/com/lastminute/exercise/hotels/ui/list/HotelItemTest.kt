package com.lastminute.exercise.hotels.ui.list

import com.lastminute.exercise.TestUtils
import com.lastminute.exercise.data.models.Currency
import com.lastminute.exercise.data.models.Location
import org.junit.Assert.assertEquals
import org.junit.Test
import java.math.BigDecimal

class HotelItemTest {

    @Test
    fun toCode() {
        val currency = Currency.EUR
        val expectedCode = "€"
        val actualCode = currency.toCode()

        assertEquals(expectedCode, actualCode)
    }

    @Test
    fun toItem() {
        val hotel = TestUtils.generateHotel(
            id = 1,
            name = "Cortez",
            price = BigDecimal(100),
            location = Location(
                "123 str",
                "Los Angeles",
                10.1,
                10.1
            ),
            stars = 5,
            userRating = 4.9f,
            gallery = listOf("https://static.wikia.nocookie.net/americanhorrorstory/images/7/73/AHS_Hotel_Cortez_Lobby.jpeg/revision/latest/scale-to-width-down/1000?cb=20151108195516")
        )
        val expectedHotelItem = HotelItem(
            1,
            "Cortez",
            "€ 100",
            "123 str",
            "Los Angeles",
            5,
            "4.9",
            "https://static.wikia.nocookie.net/americanhorrorstory/images/7/73/AHS_Hotel_Cortez_Lobby.jpeg/revision/latest/scale-to-width-down/1000?cb=20151108195516"
        )
        val actualHotelItem = hotel.toItem()

        assertEquals(expectedHotelItem, actualHotelItem)
    }
}