package com.lastminute.exercise.domain

import com.lastminute.exercise.TestUtils
import com.lastminute.exercise.data.HotelsRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class LoadHotelDetailsUseCaseImplTest {

    private val repository: HotelsRepository = mock()
    private val useCase = LoadHotelDetailsUseCaseImpl(repository)

    @Test
    fun execute() = runBlockingTest {
        val hotel = TestUtils.generateHotel()
        whenever(repository.loadHotel(1)).thenReturn(hotel)

        val result = useCase.execute(1)

        Assert.assertTrue(result is Result.Success)
        Assert.assertEquals(hotel, (result as Result.Success).data)

        verify(repository).loadHotel(1)
    }
}