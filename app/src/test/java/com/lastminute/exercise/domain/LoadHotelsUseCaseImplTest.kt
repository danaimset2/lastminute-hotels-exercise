package com.lastminute.exercise.domain

import com.lastminute.exercise.TestUtils
import com.lastminute.exercise.data.HotelsRepository
import com.lastminute.exercise.ext.deepEqualTo
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class LoadHotelsUseCaseImplTest {

    private val repository: HotelsRepository = mock()
    private val useCase = LoadHotelsUseCaseImpl(repository)

    @Test
    fun execute() = runBlockingTest {
        val hotels = listOf(TestUtils.generateHotel())
        whenever(repository.loadHotels(false))
            .thenReturn(hotels)

        val result = useCase.execute(false)

        Assert.assertTrue(result is Result.Success)
        Assert.assertTrue(hotels deepEqualTo (result as Result.Success).data)

        verify(repository).loadHotels(false)
    }

    @Test
    fun `when error occurred while accessing repository then error`() = runBlockingTest {
        val error = IllegalStateException("Error")
        whenever(repository.loadHotels(false))
            .thenThrow(error)

        val result = useCase.execute(false)

        Assert.assertTrue(result is Result.Error)
        Assert.assertEquals((result as Result.Error).exception, error)

        verify(repository).loadHotels(false)
    }

    @Test
    fun `when request reload hotels from UI then consider it in domain`() = runBlockingTest {
        val hotels = listOf(TestUtils.generateHotel())
        whenever(repository.loadHotels(true))
            .thenReturn(hotels)

        val result = useCase.execute(true)

        Assert.assertTrue(result is Result.Success)
        Assert.assertTrue(hotels deepEqualTo (result as Result.Success).data)

        verify(repository).loadHotels(true)
    }
}
