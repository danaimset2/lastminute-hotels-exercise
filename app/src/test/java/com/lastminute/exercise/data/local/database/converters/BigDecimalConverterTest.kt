package com.lastminute.exercise.hotels.data.local.database.converters

import org.junit.Assert.assertEquals
import org.junit.Test
import java.math.BigDecimal

class BigDecimalConverterTest {

    @Test
    fun from() {
        val bigDecimalJson = "100.50"
        val expectedBigDecimal = BigDecimal("100.50")
        val actualBigDecimal = BigDecimalConverter.from(bigDecimalJson)

        assertEquals(expectedBigDecimal, actualBigDecimal)
    }

    @Test
    fun to() {
        val bigDecimal = BigDecimal("100")
        val expectedJson = "100"
        val actualJson = BigDecimalConverter.to(bigDecimal)

        assertEquals(expectedJson, actualJson)
    }
}