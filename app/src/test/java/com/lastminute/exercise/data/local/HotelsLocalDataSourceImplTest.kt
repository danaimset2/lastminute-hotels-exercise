package com.lastminute.exercise.hotels.data.local

import com.lastminute.exercise.TestUtils
import com.lastminute.exercise.hotels.data.local.database.dao.ContactDao
import com.lastminute.exercise.hotels.data.local.database.dao.HotelDao
import com.lastminute.exercise.hotels.data.local.database.dao.LocationDao
import com.lastminute.exercise.hotels.data.local.database.dao.PhotoDao
import com.lastminute.exercise.hotels.data.local.database.dao.TimeRangeDao
import com.lastminute.exercise.hotels.data.local.database.models.HotelWithRelations
import com.lastminute.exercise.ext.deepEqualTo
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class HotelsLocalDataSourceImplTest {

    private val contactDao: ContactDao = mock()
    private val locationDao: LocationDao = mock()
    private val timeRangeDao: TimeRangeDao = mock()
    private val photoDao: PhotoDao = mock()
    private val hotelDao: HotelDao = mock()
    private val dataSource = HotelsLocalDataSourceImpl(
        contactDao,
        locationDao,
        timeRangeDao,
        photoDao,
        hotelDao
    )

    private val hotels = listOf(
        TestUtils.generateHotelWithRelations(TestUtils.generateHotelEntity(1)),
        TestUtils.generateHotelWithRelations(TestUtils.generateHotelEntity(2))
    )

    @Test
    fun saveHotels() = runBlockingTest {
        dataSource.saveHotels(listOf())

        verify(contactDao).insertAll(any())
        verify(locationDao).insertAll(any())
        verify(timeRangeDao).insertAll(any())
        verify(photoDao).insertAll(any())
        verify(hotelDao).insertAll(any())
    }

    @Test
    fun loadHotels() = runBlockingTest {
        whenever(hotelDao.queryAll()).thenReturn(hotels)

        val loadedHotels = dataSource.loadHotels()

        assertTrue(hotels.map(HotelWithRelations::toHotel) deepEqualTo loadedHotels)

        verify(hotelDao).queryAll()
    }

    @Test
    fun loadHotel() = runBlockingTest {
        whenever(hotelDao.queryById(1)).thenReturn(hotels.first())

        val loadedHotel = dataSource.loadHotel(1)

        assertEquals(hotels.first().toHotel(), loadedHotel)

        verify(hotelDao).queryById(1)
    }

    @Test
    fun clearAll() = runBlockingTest {
        dataSource.clearAll()

        verify(hotelDao).clearAll()
    }
}