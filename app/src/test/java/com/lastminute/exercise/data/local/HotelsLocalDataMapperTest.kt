package com.lastminute.exercise.hotels.data.local

import com.lastminute.exercise.TestUtils
import com.lastminute.exercise.hotels.data.local.database.entities.ContactEntity
import com.lastminute.exercise.hotels.data.local.database.entities.HotelEntity
import com.lastminute.exercise.hotels.data.local.database.entities.LocationEntity
import com.lastminute.exercise.hotels.data.local.database.entities.PhotoEntity
import com.lastminute.exercise.hotels.data.local.database.entities.TimeRangeEntity
import com.lastminute.exercise.hotels.data.local.database.entities.TimeRangeType
import com.lastminute.exercise.data.models.Contact
import com.lastminute.exercise.data.models.Currency
import com.lastminute.exercise.data.models.Hotel
import com.lastminute.exercise.data.models.Location
import com.lastminute.exercise.data.models.TimeRange
import com.lastminute.exercise.ext.deepEqualToIgnoreOrder
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import java.math.BigDecimal

class HotelsLocalDataMapperTest {

    private val hotel = Hotel(
        1,
        "Cortez",
        Location(
            "317 South Spring Street",
            "Los Angeles",
            34.0500096,
            -118.2496724
        ),
        5,
        TimeRange("00:00", "12:00"),
        TimeRange("14:00", "20:00"),
        Contact(
            "+14154006845",
            "danaimset@gmail.com"
        ),
        listOf(
            "https://static.wikia.nocookie.net/americanhorrorstory/images/7/73/AHS_Hotel_Cortez_Lobby.jpeg/revision/latest/scale-to-width-down/1000?cb=20151108195516",
            "https://static.wikia.nocookie.net/americanhorrorstory/images/0/0b/Hotel_Cortez_Lobby_001.jpeg/revision/latest/scale-to-width-down/1000?cb=20151011140407",
            "https://static.wikia.nocookie.net/americanhorrorstory/images/0/07/Hotel_Cortez_Lobby_002.jpeg/revision/latest/scale-to-width-down/1000?cb=20151011140515"
        ),
        5f,
        BigDecimal(100),
        Currency.EUR
    )

    @Test
    fun toEntity() {
        val expectedHotel = HotelEntity(
            1,
            "Cortez",
            5,
            5f,
            BigDecimal(100),
            Currency.EUR
        )
        val actualHotel = hotel.toEntity()

        assertEquals(expectedHotel, actualHotel)
    }

    @Test
    fun contactEntity() {
        val expectedContact = ContactEntity(
            0,
            1,
            "+14154006845",
            "danaimset@gmail.com"
        )
        val actualContact = hotel.contactEntity()

        assertEquals(expectedContact, actualContact)
    }

    @Test
    fun locationEntity() {
        val expectedLocation = LocationEntity(
            0,
            1,
            "317 South Spring Street",
            "Los Angeles",
            34.0500096,
            -118.2496724
        )
        val actualLocation = hotel.locationEntity()

        assertEquals(expectedLocation, actualLocation)
    }

    @Test
    fun timeRanges() {
        val expectedTimeRanges = listOf(
            TimeRangeEntity(
                0,
                1,
                "00:00",
                "12:00",
                TimeRangeType.CHECK_IN
            ),
            TimeRangeEntity(
                0,
                1,
                "14:00",
                "20:00",
                TimeRangeType.CHECK_OUT
            )
        )
        val actualTimeRanges = hotel.timeRanges()

        assertTrue(expectedTimeRanges deepEqualToIgnoreOrder actualTimeRanges)
    }

    @Test
    fun photos() {
        val photoEntity = PhotoEntity(
            0,
            1,
            ""
        )
        val expectedPhotos = listOf(
            photoEntity.copy(url = "https://static.wikia.nocookie.net/americanhorrorstory/images/7/73/AHS_Hotel_Cortez_Lobby.jpeg/revision/latest/scale-to-width-down/1000?cb=20151108195516"),
            photoEntity.copy(url = "https://static.wikia.nocookie.net/americanhorrorstory/images/0/0b/Hotel_Cortez_Lobby_001.jpeg/revision/latest/scale-to-width-down/1000?cb=20151011140407"),
            photoEntity.copy(url = "https://static.wikia.nocookie.net/americanhorrorstory/images/0/07/Hotel_Cortez_Lobby_002.jpeg/revision/latest/scale-to-width-down/1000?cb=20151011140515"),
        )
        val actualPhotos = hotel.photos()

        assertTrue(expectedPhotos deepEqualToIgnoreOrder actualPhotos)
    }

    @Test
    fun toHotel() {
        val hotelWithRelations = TestUtils.generateHotelWithRelations()
        val expectedHotel = TestUtils.generateHotel(
            1,
            "Hotel",
            Location(
                "123 str",
                "Big city",
                50.0,
                50.0,
            ),
            1,
            TimeRange("10:00", "12:00"),
            TimeRange("10:00", "12:00"),
            Contact(
                "+333-333-33-33",
                "hotel@gmail.com"
            ),
            listOf("link"),
            2.5f,
            BigDecimal(100),
            Currency.EUR
        )
        val actualHotel = hotelWithRelations.toHotel()

        assertEquals(expectedHotel, actualHotel)
    }
}
