package com.lastminute.exercise.splash

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.lastminute.exercise.MainCoroutineRule
import com.lastminute.exercise.hotels.data.local.HotelsLocalDataSource
import com.lastminute.exercise.getOrAwaitValue
import com.lastminute.exercise.observeForTesting
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

@ExperimentalCoroutinesApi
class SplashViewModelTest {

    private val hotelsLocalDataSource: HotelsLocalDataSource = mock()
    private val viewModel: SplashViewModel = SplashViewModel(hotelsLocalDataSource)

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @Test
    fun cleanUp() = mainCoroutineRule.runBlockingTest {
        viewModel.cleanUp()

        viewModel.startUpEvent.observeForTesting {
            Assert.assertEquals(viewModel.startUpEvent.getOrAwaitValue(), Unit)
        }

        verify(hotelsLocalDataSource).clearAll()
    }
}