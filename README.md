** Description **  
Mobile Coding Exercise  
Create a mobile app that displays the list and the details of the hotels contained in the response. Feel free to add any feature that you feel relevant to the use case.  
The endpoint to get the list of hotels is https://run.mocky.io/v3/eef3c24d-5bfd-4881-9af7-0b404ce09507 - each item includes:  
- Name  
- Number of stars  
- User rating  
- Full address with city and coordinates  
- Check-in and check-out times  
- Email address and phone number  
- Urls of some images  
- Price per night with currency  
**Guidelines:**  
- You can use any of these platforms: React Native/iOS/Android  
- Feel free to express your creativity with the UX/UI  
- No need to worry about legacy or tablet support  
- You can use third-party libraries if you want  
- Nice to have: filter or sort for the list based at least on one dimension  

** Source **  

- The main source code is under **develop** branch.  
- Others branches exist just to provide the app changes history  

** Installing **  

./gralew installDebug  

** Testing **  

*./gardlew testDebugUnitTest* for unit tests  

*./gardlew connectedAndroidTest* for instrumentation tests  

** Assumptions **  
1. API is solid and does not contain optional(nullable) fields  
2. Data stored locally and available from cache until the user swipe and refresh list of hotels or restart the app  

** Limitations **  
1. Basic UI was implemented, no transition effects, no fancy animations no benchmark & optimizations for large lists etc.  
2. Due to the lack of time filtering/sorting were not implemented, gallery preview wasn't implemented  